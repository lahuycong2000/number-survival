using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Ensign;
using Ensign.Unity;
using NumberSurvival;
using System.Linq;
using UnityEngine.SceneManagement;
public class GameManager : Singleton<GameManager>
{
    public const string PATH_DATA_SKILL = "Data/SkillData";
    public const string PATH_DATA_PLAYER = "Data/PlayerData";
    public const string PATH_UPGRADE_SKILL = "Data/UpgradeSkillData";
    public const string PATH_SKILL = "Item/SkillIcon/{0}";
    [SerializeField] private PlayerManager _playerManager;
    [SerializeField] private GameplayUIManager _UIManager;
    [SerializeField] private LevelManager _levelManager;
    [SerializeField] private List<ItemExp> _itemExp = new List<ItemExp>();
    [SerializeField] private List<Enemy> _enemyPrefabs = new List<Enemy>();
    [SerializeField] private List<Enemy> _enemyBoss = new List<Enemy>();
    [SerializeField] private ProjectileController _projectile;
    [SerializeField] private TextDamage _txtDamage;
    [SerializeField] private Ball _ball;
    [SerializeField] private Button _pauseButton;
    [SerializeField] private float _spawnRadius;
    [SerializeField] private int _countEnemyStart;
    [SerializeField] private UpgradeController _upgrade;
    [SerializeField] private VfxManager _vfxManager;
    [SerializeField] private PlayerData _playerData = new PlayerData();

    [SerializeField] private UserData _userData = new UserData();
    [SerializeField] private DataSkill _dataSkill = new DataSkill();
    [SerializeField] private DataSkill _dataUpSkill = new DataSkill();
    [SerializeField] private List<int> _numberSkillGet = new List<int>();
    // [SerializeField]private List<int> _numberSkillSet = new List<int>();
    private List<SkillLvl> _skillLvl = new List<SkillLvl>();

    private bool _isWin = false;
    private List<ItemExp> _currentListExp = new List<ItemExp>();
    private List<Enemy> _aliveEnemies = new List<Enemy>();
    private EGameState _gameState;
    private Vector2 _screenBounds;
    private int _currentEnemyKilled;
    private float _coinEarned;
    private bool _isBoss = true;
    public Canvas canvas;

    public TextDamage TxtDamage { get => _txtDamage; set => _txtDamage = value; }
    public VfxManager VFXManager { get => _vfxManager; set => _vfxManager = value; }
    public Ball BallController { get => _ball; set => _ball = value; }
    public DataSkill DataSkill { get => _dataSkill; set => _dataSkill = value; }
    public DataSkill DataUpdateSkill { get => _dataUpSkill; set => _dataUpSkill = value; }
    public List<int> NumberSkillGet { get => _numberSkillGet; set => _numberSkillGet = value; }
    public PlayerData PlayerData { get => _playerData; set => _playerData = value; }
    public bool IsBoss { get => _isBoss; set => _isBoss = value; }
    public bool IsWin
    {
        get => _isWin;
        set => _isWin = value;
    }
    public PlayerManager PlayerManager
    {
        get => _playerManager;
        set => _playerManager = value;
    }
    public int CurrentEnemyKilled
    {
        get
        {
            return _currentEnemyKilled;
        }
        set
        {
            _currentEnemyKilled = value;
            // _levelManager.ReloadUI();
        }
    }
    public float CoinEarned
    {
        get
        {
            return _coinEarned;
        }
        set
        {
            _coinEarned = value;
        }
    }
    public UserData UserData
    {
        get => _userData;
        set => _userData = value;
    }
    public LevelManager LevelManager
    {
        get => _levelManager;
        set => _levelManager = value;
    }
    public UpgradeController UpgradeController
    {
        get => _upgrade;
        set => _upgrade = value;
    }
    public List<ItemExp> CurrentListExp
    {
        get => _currentListExp;
        set => _currentListExp = value;
    }
    public List<Enemy> AliveEnemies
    {
        get => _aliveEnemies;
        set => _aliveEnemies = value;
    }
    public EGameState GameState
    {
        get
        {
            return _gameState;
        }
        set
        {
            _gameState = value;
            switch (_gameState)
            {
                case EGameState.MainMenu:
                    _levelManager.gameObject.SetActive(false);
                    _UIManager.gameObject.SetActive(true);
                    if (AliveEnemies.Count != 0)
                    {
                        AliveEnemies.ForEach(item => item.gameObject.Recycle());
                    }
                    AliveEnemies.Clear();
                    if (CurrentListExp.Count != 0)
                    {
                        CurrentListExp.ForEach(item => item.gameObject.Recycle());
                    }
                    CurrentListExp.Clear();
                    _ball.Reset();
                    _playerManager.IsRailGun = false;
                    _playerManager.IsLighting = true;
                    ReadDataJSON();
                    break;
                case EGameState.StartGame:
                    AudioManager.Instance.PlayMusic("Theme2");
                    _playerManager.StartInit();
                    _playerManager.HealthBar.gameObject.SetActive(true);
                    CoinEarned = 0;
                    _UIManager.gameObject.SetActive(false);
                    _levelManager.gameObject.SetActive(true);
                    PlayerManager.HealthPlayer();
                    SpawnEnemy();
                    _itemExp.ForEach(item => item.CreatePool<ItemExp>(10));
                    _enemyPrefabs.ForEach(item => item.CreatePool<Enemy>(100));
                    _projectile.CreatePool<ProjectileController>(50);
                    _txtDamage.CreatePool<TextDamage>(50);
                    _vfxManager.InitVFX();
                    SetData();
                    GameState = EGameState.Playing;

                    break;
                case EGameState.PauseGame:
                    // if (_playerManager != null)
                    //     _playerManager.MovementJoystick.BackJoystick();
                    break;
                case EGameState.ResumeGame:

                    break;
                case EGameState.PauseMenu:
                    AudioManager.Instance.PlaySfx("ButtonClick");
                    PausePopup();
                    break;
                case EGameState.Playing:

                    break;
                case EGameState.EndGame:
                    _playerManager.HealthBar.gameObject.SetActive(false);
                    PopupEndGame popup = PopupOpenerSingleton.Instance.OpenPopup<PopupEndGame>("UI/Popup/PopupEndGame");
                    popup.InitEndGame(_levelManager.CurrentLevel);
                    break;
            }
        }
    }
    private void Awake()
    {
        ReadDataJSON();
        if (!PlayerPrefs.HasKey("Data"))
        {
            if (_userData.levelSkill.Count == 0)
            {
                for (int i = 0; i < _dataUpSkill.skills.Length; i++)
                {
                    _userData.levelSkill.Add(0);
                }
            }
            _userData.coin = 0;
            if (_userData.shipId.Count == 0)
            {
                _userData.shipId.Add(0);
                _userData.shipIdSelect = 0;
                Debug.Log(1);
            }

            SaveNewDataPlayer();
            SaveData();
        }
        else
        {
            LoadData();
            PlayerData = UserData.playerData;
            Debug.Log(3);
        }
        GameManager.Instance.PlayerManager.PlayerRender(_userData.shipIdSelect);

    }
    public void SaveData()
    {
        string data = JsonUtility.ToJson(_userData);
        PlayerPrefs.SetString("Data", data);
    }
    public void LoadData()
    {
        string data = PlayerPrefs.GetString("Data");
        _userData = JsonUtility.FromJson<UserData>(data);
    }
    public Sprite LoadImage(string name)
    {
        string path = string.Format(PATH_SKILL, name);
        Sprite sprite = Resources.Load<Sprite>(path);
        return sprite;
    }
    public void ReadDataJSON()
    {
        var dataJson = Resources.Load<TextAsset>(PATH_DATA_SKILL);
        _dataSkill = JsonUtility.FromJson<DataSkill>(dataJson.text);
        foreach (var item in _dataSkill.skills)
        {
            SkillLvl lvl = new SkillLvl();
            lvl.nameSkill = item.nameSkill;
            lvl.lvl = 0;
            _skillLvl.Add(lvl);
        }
        var upgradeSkillData = Resources.Load<TextAsset>(PATH_UPGRADE_SKILL);
        _dataUpSkill = JsonUtility.FromJson<DataSkill>(upgradeSkillData.text);
    }
    public void SaveNewDataPlayer()
    {
        PlayerData newData = new PlayerData();
        newData.playerLevel = 1;
        newData.playerDie = false;

        newData.playerDamage = 10;
        newData.playerSpeed = 150;
        newData.playerMaxHealth = 200;
        newData.playerCurrenHealth = 200;
        newData.distanceGetEXP = 1;
        newData.distanceShootEnemy = 3;
        newData.timeFireRate = 1;
        newData.armor = 0;
        newData.expBonus = 0;
        newData.coinBonus = 0;
        newData.timeFireRateRailGun = 3;
        newData.damageRailGun = 50;
        newData.healthIndex = 1;
        PlayerData = newData;
        UserData.playerData = newData;

    }
    public void SetData()
    {
        for (int i = 0; i < GameManager.Instance.DataSkill.skills.Length; i++)
        {
            _numberSkillGet.Add(i);
        }
    }
    public Vector2 SpawnPos()
    {
        Vector2 spawnPos = _playerManager.transform.position;
        spawnPos += Random.insideUnitCircle.normalized * _spawnRadius;
        return spawnPos;
    }
    private float _inHP = 10f;
    public void SpawnEnemy()
    {
        int _enemyCount = 1;
        if (_aliveEnemies.Count == 0)
        {
            _enemyCount = 15;
        }
        for (int i = 0; i < _enemyCount; i++)
        {
            Enemy enemy = _enemyPrefabs.PickRandom().Spawn(SpawnPos());
            _aliveEnemies.Add(enemy);
            enemy.InitEnemy(_inHP, _inHP / 10);
            _inHP += 1f;
        }

    }
    
    public void SpawnBoss()
    {
        Debug.Log("run");
        Enemy enemy = _enemyBoss.PickRandom().Spawn(SpawnPos());
        _aliveEnemies.Add(enemy);
        enemy.InitEnemy(1, 100, true);
    }
    public void SpawnExpGold(Vector3 positionSpawn)
    {
        ItemExp item = _itemExp.PickRandom().Spawn(positionSpawn);
        _currentListExp.Add(item);
    }
    private void OnEnable()
    {
        _pauseButton.onClick.AddListener(() =>
        {
            GameState = EGameState.PauseMenu;
        });
    }
    private void OnDisable()
    {
        _pauseButton.onClick.RemoveListener(() =>
        {
            GameState = EGameState.PauseMenu;
        });
    }
    public void PausePopup()
    {
        Popup popup = PopupOpenerSingleton.Instance.OpenPopup<PausePopup>("UI/Popup/PausePopup");
    }

    public SkillLvl GetDataSkill(string name)
    {
        return _skillLvl.FirstOrDefault(s => s.nameSkill == name);
    }
    public void Reset()
    {
        SceneManager.LoadScene("GameScene");
    }

}
public enum EGameState
{
    MainMenu,
    StartGame,
    Playing,
    PauseGame,
    ResumeGame,
    PauseMenu,
    EndGame
}
public class SkillLvl
{
    public string nameSkill;
    public int lvl;
}
