using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System;
using Ensign;
using Ensign.UniRx;

public class TestPopup : Popup
{
    [SerializeField] private Button _btnClose;
    private Subject<bool> _onNextButtonClick = new Subject<bool>();
    public IObservable<bool> OnNextButtonClickedAsObserver => _onNextButtonClick;

    private void OnEnable()
    {
        _btnClose.onClick.AddListener(() =>
       {
           this.Close();
       });
    }
    private void OnDisable()
    {
        _btnClose.onClick.RemoveListener(() => this.Close());
    }
}
