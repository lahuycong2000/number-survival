using System;
using System.Collections.Generic;
using NumberSurvival;
[Serializable]
public class UserData
{
    public float coin;
    public List<int> shipId = new List<int>();
    public int shipIdSelect;
    public List<int> levelSkill = new List<int>();
    public PlayerData playerData;
}
