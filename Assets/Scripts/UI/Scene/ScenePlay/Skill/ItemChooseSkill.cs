using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using NumberSurvival;

using System;
public class ItemChooseSkill : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _nameSkill;
    [SerializeField] private TextMeshProUGUI _levelSkill;
    [SerializeField] private TextMeshProUGUI _description;
    [SerializeField] private Image _iconSkill;
    [SerializeField] private Button _buttonChoose;

    private Skill _skill;

    public void Init(Skill skill)
    {
        _skill = skill;
        _nameSkill.text = skill.nameSkill.ToString();
        _levelSkill.text = GameManager.Instance.GetDataSkill(skill.nameSkill).lvl.ToString();
        _iconSkill.sprite = GameManager.Instance.LoadImage(skill.nameSkill);
        _description.text = skill.levelSkill[0].inforSkill;
    }

    public void AddListener(Action oncompleted)
    {
        _buttonChoose.onClick.AddListener(() =>
        {
            OnChooseSkill(oncompleted);
        });
    }
    public void RemoveListener(Action oncompleted)
    {
        _buttonChoose.onClick.RemoveListener(() =>
        {
            OnChooseSkill(oncompleted);
        });
    }
    public void OnChooseSkill(Action oncompleted)
    {
        PlayerData data = GameManager.Instance.PlayerManager.PlayerData;
        switch (_skill.nameSkill)
        {
            case "Damage":
                data.playerDamage += 50;
                Debug.Log("Add Damage ");
                break;
            case "Max HP":
                data.playerMaxHealth += 100;
                Debug.Log("Add Max HP ");
                break;
            case "Move speed":
                data.playerSpeed += 10;
                Debug.Log("Add Move speed");
                break;
            case "Reroll":
                data.reroll += 1;
                Debug.Log("Add Reroll");
                break;
            case "Regen":
                // cộng máu theo giây
                if (data.healthIndex == 1)
                {
                    data.healthIndex = 5;
                }
                else
                {
                    data.healthIndex += 5;
                }
                Debug.Log("Add Regen");
                break;
            case "Magnet":
                data.distanceGetEXP += 0.1f;
                Debug.Log("Add Magnet");
                break;
            case "Exp up":
                data.expBonus += 20;
                Debug.Log("Add Exp up");
                break;
            case "Coin Gain":
                data.coinBonus += 10;
                Debug.Log("Add Coin Gain");
                break;
            case "Heal":
                GameManager.Instance.PlayerManager.HealthPlayer();
                Debug.Log("Heal");
                break;
            case "Armor":
                data.armor += 5;
                Debug.Log("Add Armor");
                break;
            case "Range":
                data.distanceShootEnemy += 0.2f;
                Debug.Log("Add Barage");
                break;
            case "Lightning":

                GameManager.Instance.PlayerManager.IsLighting = true;
                data.playerDamage += 25;
                Debug.Log("Add Lightning");
                break;
            case "ORBS":
                // đặc biệt
                GameManager.Instance.BallController.StartBall(1);
                Debug.Log("Add ORBS");
                break;
            case "Railgun":
                // đặc biệt
                GameManager.Instance.PlayerManager.IsRailGun = true;
                if (data.timeFireRateRailGun > 0.5f)
                    data.timeFireRateRailGun -= 0.3f;
                data.damageRailGun += 100;
                Debug.Log("Add Railgun");
                break;
            case "Attack speed":
                data.timeFireRate -= 0.2f;
                Debug.Log("Add Attack speed");
                break;
        }
        GameManager.Instance.GetDataSkill(_skill.nameSkill).lvl += 1;
        data.playerDamage += 5;
        oncompleted.Invoke();
    }
}
