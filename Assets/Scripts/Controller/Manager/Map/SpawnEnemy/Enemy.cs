using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Ensign.UniRx;
using System;
using TMPro;
using Ensign.Unity;
using Ensign;
namespace NumberSurvival
{
    public class Enemy : MonoBehaviour
    {
        [SerializeField] private LayerMask _playerLayer;
        private float _enemySpeed;
        [SerializeField] private float _maxHealth;
        private float _currentHealth;
        private float _damage;
        [SerializeField] private TextMeshProUGUI _textHealth;
        public Canvas canvas;
        private PlayerManager _player;
        private Rigidbody2D _rb2D;
        private bool isBossss = false;
        private float _distanceWithPlayer;
        public float DistanceWithPlayer
        {
            get => _distanceWithPlayer;
            set => _distanceWithPlayer = value;
        }
        public float CurrentHealth
        {
            get
            {
                return _currentHealth;
            }
            set
            {
                _currentHealth = value;
                if (CurrentHealth <= 0)
                {
                    EnemyDie();
                }
            }
        }
        public void Awake()
        {
            _rb2D = GetComponent<Rigidbody2D>();
            gameObject.AddComponent<PolygonCollider2D>();
        }
        public void InitEnemy(float maxHealth, float damage, bool isBoss = false)
        {
            if (!isBoss)
            {
                _maxHealth = maxHealth;
            }
            _damage = damage;
            CurrentHealth = _maxHealth;
            if (isBoss)
            {
                _enemySpeed = 80f;
                isBossss = true;
            }
            else
            {
                _enemySpeed = CheckSpeed(maxHealth);
                ScaleEnemy(_maxHealth);
            }
           _textHealth.text = Mathf.FloorToInt(CurrentHealth).ToString();
            this.gameObject.SetActive(true);
        }
        public float CheckSpeed(float maxHealth)
        {
            float[] speedRanges = { 50f, 55f, 60f, 65f, 70f };
            float[] healthThresholds = { 50f, 200f, 500f, 1000f };

            for (int i = 0; i < healthThresholds.Length; i++)
            {
                if (maxHealth < healthThresholds[i])
                {
                    float minSpeed = (i == 0) ? 10f : speedRanges[i - 1];
                    float maxSpeed = speedRanges[i];
                    return UnityEngine.Random.Range(minSpeed, maxSpeed);
                }
            }

            return speedRanges[speedRanges.Length - 1];
        }
        private void OnEnable()
        {
            // CurrentHealth = _maxHealth;
            _player = GameManager.Instance.PlayerManager;
            //_textHealth.text = CurrentHealth.ToString();
        }
        public void ScaleEnemy(float maxHealth)
        {
            if (maxHealth < 50)
            {
                this.transform.localScale = this.transform.localScale * 2;
            }
            else
            {
                float total = maxHealth / 150;
                if (total >= 2)
                    this.transform.localScale = this.transform.localScale * 2.5f;
                else
                    this.transform.localScale = this.transform.localScale * total;
            }
        }
        public void FixedUpdate()
        {
            Move();
        }
        public void Move()
        {
            DistanceWithPlayer = Vector2.Distance(this.transform.position, GameManager.Instance.PlayerManager.transform.position);
            if (GameManager.Instance.GameState == EGameState.Playing)
            {
                _rb2D.velocity = (_player.transform.position - this.transform.position).normalized * _enemySpeed * Time.deltaTime;
            }
            else
            {
                _rb2D.velocity = Vector2.zero;
            }
        }
        public void EnemyDie()
        {
            GameManager.Instance.CurrentEnemyKilled++;
            AudioManager.Instance.PlaySfx("Death");
            VfxController vfx = GameManager.Instance.VFXManager.VfxEnemyDie.PickRandom().Spawn(this.transform.position);
            vfx.EndVFX(vfx);
            GameManager.Instance.SpawnExpGold(this.transform.position);
            this.Recycle();
            GameManager.Instance.AliveEnemies.Remove(this);
            // if (!GameManager.Instance.isBoss)
            // {
            GameManager.Instance.SpawnEnemy();

            if (isBossss)
            {
                GameManager.Instance.IsWin = true;
                GameManager.Instance.GameState = EGameState.EndGame;
            }
        }
        private void OnCollisionEnter2D(Collision2D other)
        {
            if ((_playerLayer.value == (1 << other.gameObject.layer)))
            {
                other.gameObject.GetComponentInParent<PlayerManager>().TakeDamage(_damage);
            }
        }
        public void TakeDamage(float damage)
        {
            CurrentHealth = CurrentHealth - damage;
            _textHealth.text = CurrentHealth.ToString();
        }
        // private void OnEnable()

    }
}