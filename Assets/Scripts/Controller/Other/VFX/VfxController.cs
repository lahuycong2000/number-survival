using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

using Ensign.Unity;
namespace NumberSurvival
{
    public class VfxController : MonoBehaviour
    {
        [SerializeField] private ParticleSystem _particle;
        
        private void OnEnable()
        {
            _particle.Play();
        }
        public void EndVFX(VfxController itemVFX)
        {
            itemVFX.ActionWaitTime(0.5f, () =>
            {
                itemVFX.Recycle();
            });
        }
    }
}

