using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Ensign;

namespace NumberSurvival
{
    public class JoyManager : MonoBehaviour, IPointerUpHandler, IPointerDownHandler, IDragHandler
    {
        [SerializeField] private GameObject _joystick;
        [SerializeField] private GameObject _joystickBG;
        private Vector2 _joystickVec;
        private Vector2 _joystickTouchPos;
        private Vector2 _joystickOriginalPos;
        private float _joystickRadius;
        public Vector2 JoystickVec { get => _joystickVec; set => _joystickVec = value; }

        void Start()
        {
            SetActiveJoy(false);
            _joystickOriginalPos = _joystickBG.transform.position;
            _joystickRadius = _joystickBG.GetComponent<RectTransform>().sizeDelta.y * 3;
            //_isNewTouch = false;
        }
        public void SetActiveJoy(bool isJoystick)
        {
            _joystick.gameObject.SetActive(isJoystick);
            _joystickBG.gameObject.SetActive(isJoystick);
        }
        public void OnPointerDown(PointerEventData eventData)
        {
            if (Input.touchCount > 1)
            {
                return;
            }
            else
            {
                _joystick.transform.position = eventData.position;
                _joystickBG.transform.position = eventData.position;
                _joystickTouchPos = eventData.position;
                SetActiveJoy(true);
            }
        }
        public void OnDrag(PointerEventData eventData)
        {
            Vector2 dragPos = eventData.position;
            _joystickVec = (dragPos - _joystickTouchPos).normalized;

            float joystickDist = Vector2.Distance(dragPos, _joystickTouchPos);

            if (joystickDist < _joystickRadius)
            {
                _joystick.transform.position = _joystickTouchPos + _joystickVec * joystickDist;
            }
            else
            {
                _joystick.transform.position = _joystickTouchPos + _joystickVec * _joystickRadius;
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
           BackJoystick();
        }
        public void BackJoystick()
        {
            _joystickVec = Vector2.zero;
            _joystick.transform.position = _joystickOriginalPos;
            _joystickBG.transform.position = _joystickOriginalPos;
            SetActiveJoy(false);
        }

    }
}
