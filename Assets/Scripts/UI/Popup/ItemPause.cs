using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ItemPause : MonoBehaviour
{
    [SerializeField] private Image _imageItem;
    [SerializeField] private TextMeshProUGUI _txtLv;
    public void InitPause(Sprite img, string text)
    {
        _imageItem.sprite = img;
        _txtLv.text = "Lv " + text;
    }

}
