using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Ensign.Unity;
public class HealthBar : MonoBehaviour
{
    [SerializeField] private Slider _slider;
    [SerializeField] private Image _fill;
    [SerializeField] private Sprite _maxHealthColor;
    [SerializeField] private Sprite _minHealthColor;
    [SerializeField] private Image _sliderColor;
    private float _health;
    public bool isHeal = false;
    public void SetMaxHealth(float health)
    {
        _slider.maxValue = health;
        _slider.value = health;
    }
    public void SetCurHealth(float health)
    {
        _health = health;
        if (isHeal)
        {
            InvokeRepeating(nameof(DecreaseHealth), 1f, 1f);
        }
    }
    public void DecreaseHealth()
    {
        // TextDamage txt = GameManager.Instance.TxtDamage.Spawn(GameManager.Instance.canvas.transform);
        // txt.Show(_health, Color.black, true);
        _slider.value += _health;
        GameManager.Instance.PlayerManager.isHeal = true;
        GameManager.Instance.PlayerManager.CurrentHealth += _health;
        if (_slider.value > _slider.maxValue)
        {
            _slider.value = _slider.maxValue;
        }
        SetColor();
    }
    public void SetHealth(float health)
    {
        _slider.value = Mathf.Round(health);
        SetColor();
    }
    public void SetColor()
    {
        if (_slider.value < _slider.maxValue / 2)
        {
            _sliderColor.sprite = _minHealthColor;
        }
        else
        {
            _sliderColor.sprite = _maxHealthColor;
        }
    }
}
