using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using Ensign.Unity;
using Ensign.Tween;
using TMPro;
public class PlayerSelection : MonoBehaviour
{
    [SerializeField] private List<PlayerIcon> _listPlayerIcon = new List<PlayerIcon>();
    [SerializeField] private Button _btnPrevious;
    [SerializeField] private Button _btnNext;
    [SerializeField] private ButtonCustom _btnUnlock;
    [SerializeField] private ScrollRect _worldScrollRect;
    [SerializeField] private RectTransform _content;
    [SerializeField] private TextMeshProUGUI _txtShip;
    [SerializeField] private TextMeshProUGUI _txtShipValue;
    [SerializeField] private int _count;
    [SerializeField] private TextMeshProUGUI _textCoin;
    [SerializeField] private Image _select;
    private int _currentIndex;
    private PlayerIcon _currentPlayer;
    private int _index;
    [SerializeField] private List<PlayerIcon> _activeItems;
    private List<PlayerIcon> ActiveItems
    {
        get
        {
            if (_activeItems == null)
            {
                _activeItems = new List<PlayerIcon>();
            }
            if (_activeItems.Count <= 0 && _listPlayerIcon.Count > 0)
            {
                _activeItems = _listPlayerIcon.Where(item => item.gameObject.activeSelf).ToList();
            }
            return _activeItems;
        }
    }
    private void Start()
    {
        _select.gameObject.SetActive(false);
        _btnUnlock.gameObject.SetActive(true);

        HorizontalLayoutGroup grid = _content.GetComponent<HorizontalLayoutGroup>();
        for (int i = 0; i < _count; i++)
        {
            PlayerIcon playerIcon = Resources.Load<PlayerIcon>(string.Format("Prefabs/Player/PlayerIcon {0}", i + 1));
            PlayerIcon item = Instantiate(playerIcon);
            item.transform.SetParent(_content.transform);
            _listPlayerIcon.Add(item);
        }
        float totalWith = 0;
        _listPlayerIcon.ForEach(item => totalWith += item.GetComponent<RectTransform>().GetSize().x);

        float scrollWith = _worldScrollRect.GetComponent<RectTransform>().GetSize().x;
        grid.spacing = scrollWith / 5;
        grid.padding.left = (int)scrollWith / 2;
        grid.padding.right = (int)scrollWith / 2;
        _content.SetWidth(ActiveItems.Count * grid.spacing + grid.padding.left + grid.padding.right);

        _currentPlayer = ActiveItems[0];
        _txtShip.text = _currentPlayer.NameShip;
        _txtShipValue.text = _currentPlayer.Price.ToString();
        OnDataChanged(_currentPlayer);
        CheckShip();
    }
    public void CheckShip()
    {
        for (int i = 0; i < GameManager.Instance.UserData.shipId.Count; i++)
        {
            int item = GameManager.Instance.UserData.shipId[i];
            for (int j = 0; j < _listPlayerIcon.Count; j++)
            {
                if (item == _listPlayerIcon[i].PlayerIndex)
                {
                    _listPlayerIcon[i].Unlocked = true;
                }
            }
        }
        if (_currentIndex == GameManager.Instance.UserData.shipIdSelect)
        {
            _select.gameObject.SetActive(true);
            _btnUnlock.ButtonBuy(0, _currentPlayer);
        }

    }
    private void OnEnable()
    {
        _btnPrevious.onClick.AddListener(OnPreviousItem);
        _btnNext.onClick.AddListener(OnNextItem);
        _btnUnlock.GetComponent<Button>().onClick.AddListener(OnSelect);
        _textCoin.text = GameManager.Instance.UserData.coin.ToString();
    }
    private void OnDisable()
    {
        _btnPrevious.onClick.RemoveListener(OnPreviousItem);
        _btnNext.onClick.RemoveListener(OnNextItem);
        _btnUnlock.GetComponent<Button>().onClick.RemoveListener(OnSelect);
    }
    private void OnSelect()
    {
        if (_currentPlayer.Price < GameManager.Instance.UserData.coin & !_currentPlayer.Unlocked)
        {
            Debug.Log(" _currentPlayer.Price " + _currentPlayer.Price);
            GameManager.Instance.UserData.coin -= _currentPlayer.Price;
            _currentPlayer.Unlocked = true;
            GameManager.Instance.UserData.shipId.Add(_currentIndex);
            GameManager.Instance.UserData.shipIdSelect = _currentIndex;
            GameManager.Instance.SaveData();
            _select.gameObject.SetActive(true);
            GameManager.Instance.PlayerManager.PlayerRender(_currentPlayer.PlayerIndex);
            OnDataChanged(_currentPlayer);
            CheckShip();
        }
        if (_currentPlayer.Unlocked)
        {
            GameManager.Instance.PlayerManager.PlayerRender(_currentPlayer.PlayerIndex);
        }
    }
    private void ReloadUI()
    {
        ActiveItems.ForEach(item =>
        {
            item.SetHighlightScale(_currentPlayer);
        });
        if (_currentPlayer != null)
        {

            if (_currentPlayer.Unlocked)
            {
                _btnUnlock.ButtonBuy(0, _currentPlayer);
                _index = 1;
            }
            else
            {
                _btnUnlock.ButtonBuy(1, _currentPlayer);
                _index = 3;
            }
        }
        // _skillImage.sprite = GameManager.Instance.LoadImage(_currentPlayer.CurrentSkill.nameSkill);
        _textCoin.text = GameManager.Instance.UserData.coin.ToString();
    }
    private void OnDataChanged(PlayerIcon player)
    {
        _currentPlayer = player;
        _currentIndex = player == null ? -1 : ActiveItems.IndexOf(player);
        if (_currentIndex >= ActiveItems.Count) return;
        CheckPos(_currentIndex);
        Debug.Log("_currentPlayer : " + (_currentPlayer.PlayerIndex));
        ReloadUI();
    }
    public void OnNextItem()
    {
        if (_currentIndex > ActiveItems.Count) return;
        _currentIndex++;
        _currentIndex = Mathf.Clamp(_currentIndex, 0, ActiveItems.Count - 1);
        _txtShip.text = ActiveItems[_currentIndex].NameShip;
        _txtShipValue.text = ActiveItems[_currentIndex].Price.ToString();
        _select.gameObject.SetActive(_currentIndex == GameManager.Instance.UserData.shipIdSelect);

        OnDataChanged(ActiveItems[_currentIndex]);
    }
    public void OnPreviousItem()
    {
        if (_currentIndex <= 0) return;
        _currentIndex--;
        CheckPos(_currentIndex);
        // _currentIndex = Mathf.Clamp(_currentIndex, 0, ActiveItems.Count - 1);
        _txtShip.text = ActiveItems[_currentIndex].NameShip;
        _txtShipValue.text = ActiveItems[_currentIndex].Price.ToString();
        _select.gameObject.SetActive(_currentIndex == GameManager.Instance.UserData.shipIdSelect);
        OnDataChanged(ActiveItems[_currentIndex]);
    }
    public void CheckPos(float currentIndex)
    {
        Vector2 currentPosition = _content.anchoredPosition;
        Vector2 newPosition = currentPosition;
        switch (currentIndex)
        {
            case 0:
                newPosition.x = -582f;
                // _worldScrollRect.horizontalNormalizedPosition = 0f;
                break;
            case 1:
                // _worldScrollRect.horizontalNormalizedPosition = 0.05f;
                newPosition.x = -881f;
                break;
            case 2:
                // _worldScrollRect.horizontalNormalizedPosition = 0.1f;
                newPosition.x = -1193f;
                break;
            case 3:
                // _worldScrollRect.horizontalNormalizedPosition = 0.35f;
                newPosition.x = -1529f;
                break;
            case 4:
                // _worldScrollRect.horizontalNormalizedPosition = 0.45f;
                newPosition.x = -1865f;
                break;
            case 5:
                // _worldScrollRect.horizontalNormalizedPosition = 0.67f;
                newPosition.x = -2175f;
                break;
            case 6:
                // _worldScrollRect.horizontalNormalizedPosition = 0.76f;
                newPosition.x = -2479f;
                break;
            case 7:
                // _worldScrollRect.horizontalNormalizedPosition = 0.9f;
                newPosition.x = -2784f;
                break;
        }
        _content.anchoredPosition = newPosition;

    }
}
