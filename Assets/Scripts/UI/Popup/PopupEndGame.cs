using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System;
using Ensign;
using Ensign.UniRx;
using TMPro;
public class PopupEndGame : Popup
{
    [SerializeField] private TextMeshProUGUI _namePopup;
    [SerializeField] private Slider _slider;
    [SerializeField] private GameObject _summary;
    [SerializeField] private TextMeshProUGUI _enemiesKilled;
    [SerializeField] private TextMeshProUGUI _crystalEarned;
    [SerializeField] private Button _btnClose;
    public void InitEndGame(float value)
    {
        Debug.Log("value "+value);
        _slider.value = value;
        _slider.maxValue = 34;
        if (GameManager.Instance.IsWin)
        {
            _namePopup.text = "VICTORY!";
            _slider.gameObject.SetActive(false);
            AudioManager.Instance.PlaySfx("Win");
            _summary.SetActive(true);
        }
        else
        {
            _namePopup.text = "DEFEATED!";
             AudioManager.Instance.PlaySfx("Lose");
              _slider.gameObject.SetActive(true);
            _summary.SetActive(false);

        }
        _enemiesKilled.text = GameManager.Instance.CurrentEnemyKilled.ToString();
        _crystalEarned.text = GameManager.Instance.CoinEarned.ToString();
    }
    private void OnEnable()
    {
        _btnClose.onClick.AddListener(() =>
       {
           this.Close();
       });
    }
    private void OnDisable()
    {
        _btnClose.onClick.RemoveListener(() => this.Close());
    }
    public override void Close()
    {
        base.Close();
        GameManager.Instance.Reset();
        // GameManager.Instance.GameState = EGameState.MainMenu;
    }
}
