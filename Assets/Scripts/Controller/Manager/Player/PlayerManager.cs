using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Ensign.Unity;
using System.Linq;
using Ensign.Tween;
namespace NumberSurvival
{
    public class PlayerManager : MonoBehaviour
    {
        private const string PATH_PLAYERICON = "Prefabs/space_ship_{0}";
        [SerializeField] private JoyManager _movementJoystick;
        [SerializeField] private float _playerRotateSpeed;
        [SerializeField] private SpriteRenderer _playerRenderer;
        [SerializeField] private Color _colorTakeDamage;
        [SerializeField] private ProjectileController _projectileNormal;
        [SerializeField] private ProjectileController _projectileRailGun;
        [SerializeField] private ProjectileController _projectileLighting;
        [SerializeField] private Transform _shootingPoint;
        [Header("CAMERA SETTING ")]
        [SerializeField] private Vector3 _camOffset;
        [SerializeField] private Camera _mainCamera;
        [Header("SHOOT ")]
        [SerializeField] private float _fireRate;
        [SerializeField] private float _timeToFire;
        private float _fireRateRail;
        [SerializeField] private float _timeToFireRail;
        [SerializeField] private bool _isRailGun;
        [SerializeField] private bool _isLighting;
        [Header("DAMAGE SETTING ")]
        private float _armor;
        [SerializeField] private float _currentHealth;
        [SerializeField] private HealthBar _healthBar;
        private PlayerData _playerData = new PlayerData();
        public PlayerData PlayerData
        {
            get => _playerData;
            set => _playerData = value;
        }
        public HealthBar HealthBar
        {
            get => _healthBar;
            set => _healthBar = value;
        }
        public bool IsRailGun
        {
            get => _isRailGun;
            set => _isRailGun = value;
        }
        public bool IsLighting
        {
            get => _isLighting;
            set => _isLighting = value;
        }
        public bool isHeal;
        public float CurrentHealth
        {
            get
            {
                return _currentHealth;
            }
            set
            {
                Color colorOrigin = _playerRenderer.color;
                if (!isHeal)
                {
                    if (!LeanTween.isTweening(_playerRenderer.gameObject))
                    {
                        _playerRenderer.color = _colorTakeDamage;
                    }
                    _currentHealth = value;
                    if (!LeanTween.isTweening(_playerRenderer.gameObject))
                    {
                        LeanTween.color(_playerRenderer.gameObject, colorOrigin, 0.2f);
                    }
                }
                if (_currentHealth <= 0 && !_playerData.playerDie)
                {
                    PlayerDie();
                }
            }
        }
        public JoyManager MovementJoystick
        {
            get => _movementJoystick;
            set => _movementJoystick = value;
        }
        public void StartInit()
        {
            _playerData = Data();
            _fireRate = _playerData.timeFireRate;
            Debug.Log("dddddddddddddd" + _fireRate);
            _fireRateRail = _playerData.timeFireRateRailGun;
            _armor = _playerData.armor;
            _playerData.playerDie = false;

            HealthPlayer();
        }
        public PlayerData Data()
        {
            PlayerData data = GameManager.Instance.PlayerData;
            PlayerData newData = new PlayerData();
            newData.playerLevel = 1;
            newData.playerDie = false;

            newData.playerDamage = data.playerDamage;
            newData.playerSpeed = data.playerSpeed;
            newData.playerMaxHealth = data.playerMaxHealth;
            newData.playerCurrenHealth = data.playerCurrenHealth;
            newData.distanceGetEXP = data.distanceGetEXP;
            newData.distanceShootEnemy = data.distanceShootEnemy;
            newData.timeFireRate = data.timeFireRate;
            newData.armor = data.armor;
            newData.expBonus = data.expBonus;
            newData.coinBonus = data.coinBonus;
            newData.timeFireRateRailGun = data.timeFireRate;
            newData.damageRailGun = data.damageRailGun;
            newData.healthIndex = data.healthIndex;
            return newData;
        }
        private void Update()
        {
            if (GameManager.Instance.GameState == EGameState.Playing)
            {
                CheckEnemy();
                CamFollow();
                Move();
                CheckEXPOnChange();
                if (_currentHealth < _playerData.playerMaxHealth && _currentHealth > 0
                 && !_healthBar.isHeal && GameManager.Instance.PlayerManager.PlayerData.healthIndex >= 1)
                {
                    _healthBar.isHeal = true;
                    _healthBar.SetCurHealth(GameManager.Instance.PlayerManager.PlayerData.healthIndex);
                }
                else if (_currentHealth >= _playerData.playerMaxHealth)
                {
                    CancelInvoke(nameof(_healthBar.DecreaseHealth));
                    _healthBar.isHeal = false;
                }
            }
            else
            {
                CancelInvoke(nameof(_healthBar.DecreaseHealth));
            }
        }
        public void Move()
        {
            if (_movementJoystick.JoystickVec.y != 0)
            {
                this.transform.position = new Vector2(this.transform.position.x + (_movementJoystick.JoystickVec.x 
                * _playerData.playerSpeed * Time.deltaTime) / 100,
                this.transform.position.y + (_movementJoystick.JoystickVec.y * _playerData.playerSpeed * Time.deltaTime) / 100);
                Quaternion target = Quaternion.LookRotation(_playerRenderer.transform.forward, _movementJoystick.JoystickVec);
                Quaternion rotate = Quaternion.RotateTowards(_playerRenderer.transform.rotation, target, _playerRotateSpeed 
                * Time.deltaTime);
                _playerRenderer.transform.rotation = rotate;
            }
        }
        public void HealthPlayer()
        {
            CurrentHealth = _playerData.playerMaxHealth;
            _healthBar.SetMaxHealth(CurrentHealth);
        }
        public void CamFollow()
        {
            Vector3 camPos = new Vector3(this.transform.position.x, this.transform.position.y, _camOffset.z);
            _mainCamera.transform.position = camPos;
        }
        public void CheckEXPOnChange()
        {
            if (GameManager.Instance.CurrentListExp.Count <= 0)
            {
                return;
            }
            List<ItemExp> listItem = new List<ItemExp>();
            listItem = GameManager.Instance.CurrentListExp.Where(cell => Vector2.Distance(this.transform.position, cell.transform.position) <= _playerData.distanceGetEXP).ToList();
            if (listItem.Count > 0)
            {
                listItem.ForEach(item => item.MoveToPlayer(this));
            }
        }
        public void Shoot(Transform taget, EProjectile type)
        {
            if (GameManager.Instance.GameState == EGameState.Playing)
            {
                if (type == EProjectile.normal)
                {
                    if (Time.time > _timeToFire)
                    {
                        _timeToFire = Time.time + _fireRate;
                        ProjectileController projectile = _projectileNormal.Spawn(_shootingPoint.position, Quaternion.identity);
                        Vector3 dir = taget.transform.position - _shootingPoint.position;
                        AudioManager.Instance.PlaySfx("Shoot");
                        projectile.SetUpDir(this, dir);
                        projectile.DamageSetup(_playerData.playerDamage);
                    }
                }
                else if (type == EProjectile.Lightning)
                {
                    if (Time.time > _timeToFire)
                    {
                        Debug.Log("3333");
                        _timeToFire = Time.time + _fireRate;
                        ProjectileController projectile = _projectileLighting.Spawn(_shootingPoint.position, Quaternion.identity);
                        Vector3 dir = taget.transform.position - _shootingPoint.position;
                        AudioManager.Instance.PlaySfx("Shoot");
                        projectile.SetUpDir(this, dir);
                        projectile.DamageSetup(_playerData.playerDamage);
                    }
                }
                else
                {
                    if (Time.time > _timeToFireRail)
                    {
                        _timeToFireRail = Time.time + _fireRateRail;
                        ProjectileController projectile = _projectileRailGun.Spawn(_shootingPoint.position, Quaternion.identity);
                        Vector3 dir = taget.transform.position - _shootingPoint.position;
                        AudioManager.Instance.PlaySfx("Shoot");
                        projectile.SetUpDir(this, dir);
                        projectile.DamageSetup(_playerData.playerDamage);
                    }
                }
            }
        }
        public List<Enemy> listEnemy = new List<Enemy>();
        public void CheckEnemy()
        {
            if (GameManager.Instance.AliveEnemies.Count <= 0)
            {
                return;
            }

            listEnemy = GameManager.Instance.AliveEnemies.Where(enemy => enemy.DistanceWithPlayer <= _playerData.distanceShootEnemy).ToList();
            if (listEnemy.Count > 0)
            {
                // xu ly taget enemy gan nhat
                List<Enemy> listShoots = listEnemy.OrderBy(enemy => enemy.DistanceWithPlayer).ToList();
                // for (int i = 0; i < listShoots.Count; i++)
                // {
                if (GameManager.Instance.PlayerManager.IsLighting)
                {
                    Debug.Log("1111");
                    Shoot(listShoots[0].transform, EProjectile.Lightning);
                }
                else
                {
                    Shoot(listShoots[0].transform, EProjectile.normal);
                }
                if (_isRailGun && listShoots.Count > 2)
                {
                    Shoot(listShoots[1].transform, EProjectile.railgun);
                }
             
            }

        }
        public void Hit(float damage, Vector2 hitPoint, Vector2 hitDirection)
        {
            TakeDamage(damage);
        }
        public void PlayerRender(int index)
        {
            _playerRenderer.sprite = LoadSprite(index);
        }
        public Sprite LoadSprite(int index)
        {
            string path = string.Format(PATH_PLAYERICON, index.ToString());
            Debug.Log("path :  " + path);
            return Resources.Load<Sprite>(path);
        }
        public void TakeDamage(float damage)
        {
            Debug.Log("Player Take Damage" + damage);
            Debug.Log("Player " + _armor);
            if (_armor >= 0)
            {
                float total = damage - _armor;
                if (total > 0)
                {
                    isHeal = false;
                    CurrentHealth -= total;
                }

                Debug.Log("Player Take Damage wwith Armor" + total);
            }
            else
            {
                isHeal = false;
                CurrentHealth -= damage;
                Debug.Log("Player Take Damage" + damage);
            }
            _healthBar.SetHealth(CurrentHealth);
        }
        public void PlayerDie()
        {
            if (!_playerData.playerDie)
            {
                _playerData.playerDie = true;
            }
            AudioManager.Instance.PlaySfx("Death");
            GameManager.Instance.UserData.coin += GameManager.Instance.CoinEarned;
            GameManager.Instance.SaveData();
            GameManager.Instance.GameState = EGameState.EndGame;
        }
    }
}