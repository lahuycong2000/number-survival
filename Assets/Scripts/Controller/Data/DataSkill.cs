using System;
namespace NumberSurvival
{
    [Serializable]
    public class DataSkill
    {
        public Skill[] skills;
    }
    [Serializable]
    public class Skill
    {
        public int skillID;
        public SkillType skillType;
        public int isActive;
        public string nameSkill;
        public int defaultDamage;
        public int distanceShoot;
        public LevelSkill[] levelSkill;
    }
    [Serializable]
    public class LevelSkill
    {
        public int lvl;
        public float buffSkill;
        public int needToUpgrade;
        public string inforSkill;
    }
    public enum SkillType
    {
        Active,
        Deactive
    }
}