using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using NumberSurvival;
public class ItemUpgradeSkill : MonoBehaviour
{
    [SerializeField] private Button _selectButton;
    [SerializeField] private TextMeshProUGUI _nameSkill;
    [SerializeField] private TextMeshProUGUI _valueSkill;
    [SerializeField] private Image _imageSkill;
    [SerializeField] private TextMeshProUGUI _levelSkill;
    [SerializeField] private TextMeshProUGUI _summary;
    [SerializeField] private TextMeshProUGUI _coinNeedToUpgrade;

    private Skill _skill;
    private LevelSkill _nextLevel;
    private LevelSkill _currentLevel;

    public Button SelectButton { get => _selectButton; set => _selectButton = value; }
    public TextMeshProUGUI NameSkill { get => _nameSkill; set => _nameSkill = value; }
    public Image ImageSkill { get => _imageSkill; set => _imageSkill = value; }
    public TextMeshProUGUI LevelSkill { get => _levelSkill; set => _levelSkill = value; }
    public TextMeshProUGUI Summary { get => _summary; set => _summary = value; }
    public TextMeshProUGUI CoinNeedToUpgrade { get => _coinNeedToUpgrade; set => _coinNeedToUpgrade = value; }
    public Skill Skill { get => _skill; set => _skill = value; }
    public LevelSkill NextLevel { get => _nextLevel; set => _nextLevel = value; }

    private void Start()
    {
        ReloadItem();
    }
    public void SkillLoader(Skill skill, bool isCurr)
    {
        Skill = skill;
        NameSkill.text = Skill.nameSkill;
        ImageSkill.sprite = GameManager.Instance.LoadImage(Skill.nameSkill);
        foreach (var temp in GameManager.Instance.DataSkill.skills)
        {
            if (temp.skillID == Skill.skillID)
            {
                Debug.Log("id" + GameManager.Instance.UserData.levelSkill[temp.skillID]);
                _currentLevel = GameManager.Instance.DataUpdateSkill.skills[temp.skillID].levelSkill[GameManager.Instance.UserData.levelSkill[temp.skillID]];
                //_currentLevel.lvl  = GameManager.Instance.UserData.levelSkill[Skill.skillID];
                OnChooseSkill(GameManager.Instance.DataUpdateSkill.skills[temp.skillID].nameSkill, _currentLevel.buffSkill);

                if (isCurr)
                    LevelSkill.text = _currentLevel.lvl.ToString();
                else
                    LevelSkill.text = _nextLevel.lvl.ToString();

            }
        }
        UpgradeLogic(true);
    }
    private void OnEnable()
    {
        SelectButton.onClick.AddListener(() => { UpgradeLogic(false); });
    }
    private void OnDisable()
    {
        SelectButton.onClick.RemoveListener(() => { UpgradeLogic(false); });
    }
    public void UpgradeLogic(bool isCheckNextLevel)
    {
        UserData user = GameManager.Instance.UserData;

        foreach (var temp in GameManager.Instance.DataUpdateSkill.skills)
        {
            if (Skill.nameSkill == temp.nameSkill)
            {
                int indexNextLevelSkill;
                if (_currentLevel.lvl < 5)
                {
                    indexNextLevelSkill = _currentLevel.lvl + 1;
                    _nextLevel = temp.levelSkill[indexNextLevelSkill];
                    UpdateValue(_currentLevel.buffSkill, NextLevel.buffSkill, indexNextLevelSkill);
                    if (!isCheckNextLevel)
                    {
                        if (user.coin < _nextLevel.needToUpgrade)
                        {
                            //UpdateCoinText(_nextLevel.needToUpgrade);
                            return;
                        }
                        else
                        {
                            //_currentLevel.lvl = _nextLevel.lvl;
                            Debug.Log("cur" + _currentLevel.lvl);
                            Debug.Log("next" + _nextLevel.lvl);
                            SkillLoader(Skill, false);
                            user.coin -= _nextLevel.needToUpgrade;
                            GameManager.Instance.UpgradeController.SetCoinText();
                            user.levelSkill[_skill.skillID] = _nextLevel.lvl;
                            ReloadItem();
                        }
                    }
                }
                else
                {
                    _levelSkill.text = "Max";
                    _coinNeedToUpgrade.text = "Max";
                    return;
                }
                UpdateCoinText(_nextLevel.needToUpgrade);
                GameManager.Instance.SaveData();
                return;
            }
        }
    

    }
    public void ReloadItem()
    {
        GameManager.Instance.UpgradeController.Items.ForEach(i =>
        {
            i.UpgradeLogic(true);
        });
    }
    public void UpdateCoinText(int coinNext)
    {
        CoinNeedToUpgrade.text = coinNext.ToString();
        if (coinNext > GameManager.Instance.UserData.coin)
        {
            CoinNeedToUpgrade.color = Color.red;
        }
        else
        {
            CoinNeedToUpgrade.color = Color.white;
        }
    }
    public void UpdateValue(float currentValue, float valueNext, int indexNextLevelSkill)
    {
        //float value = (valueNext / currentValue) * 100;
        //Debug.Log("Value % : " + Math.Round(value, 1));
        // _valueSkill.text = string.Format("+ {0} % ", Math.Round(value, 1).ToString());
        _valueSkill.text = _skill.levelSkill[indexNextLevelSkill - 1].inforSkill;
    }
    public void OnChooseSkill(string nameSkill, float index)
    {
        PlayerData data = GameManager.Instance.UserData.playerData;
        switch (nameSkill)
        {
            case "Damage":
                data.playerDamage += index;
                Debug.Log("Add Damage ");
                break;
            case "Max HP":
                data.playerMaxHealth += index;
                Debug.Log("Add Max HP ");
                break;
            case "Move speed":
                data.playerSpeed += index;
                Debug.Log("Add Move speed");
                break;
            case "Reroll":
                data.reroll += index;
                Debug.Log("Add Reroll");
                break;
            case "Regen":
                // cộng máu theo giây
                if (data.healthIndex == 1)
                {
                    data.healthIndex = 5;
                }
                else
                {
                    data.healthIndex += index;
                }
                Debug.Log("Add Regen");
                break;
            case "Magnet":
                data.distanceGetEXP += index;
                Debug.Log("Add Magnet");
                break;
            case "Exp up":
                data.expBonus += index;
                Debug.Log("Add Exp up");
                break;
            case "Coin Gain":
                data.coinBonus += index;
                Debug.Log("Add Coin Gain");
                break;
            case "Armor":
                data.armor += index;
                Debug.Log("Add Armor");
                break;
            case "Range":
                data.distanceShootEnemy += index;
                Debug.Log("Add Barage");
                break;
            case "Attack speed":
                data.timeFireRate += index;
                Debug.Log("Add Attack speed");
                break;
        }
    }
}
