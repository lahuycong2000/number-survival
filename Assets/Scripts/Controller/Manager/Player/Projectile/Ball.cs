using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NumberSurvival;
public class Ball : MonoBehaviour
{
    [SerializeField] private float _rotationSpeed = 50f;
    [SerializeField] private List<ProjectileController> ball = new List<ProjectileController>();
    public float _rotationDuration = 5f;
    public float _restDuration = 5f;
    private bool isRotating = true;
    private int _count = 0;
    private bool isFisrt = true;
    private float _damage = 100;
    public void Reset()
    {
        _count = 0;
        ball.ForEach(item => item.gameObject.SetActive(false));
        StopCoroutine(RotateAndRest());
        _rotationSpeed = 50f;
        _rotationDuration = 5f;
        _restDuration = 5f;
        _damage = 100;
    }
    public void StartBall(int count)
    {
        if (_count < 5)
        {
            _count += count;
            ball.ForEach(item => item.gameObject.SetActive(false));
            Init();
            // 
            // StartCoroutine(RotateAndRest());
        }
        _rotationDuration += 10f;
        _damage += 60;
        if (_rotationDuration != 0)
            _rotationDuration -= 1f;
        if (_restDuration != 0)
            _restDuration -= 1f;


    }
    public void Init()
    {
        if (_count >= 4)
        {
            _count = 4;
        }
        for (int i = 0; i < _count; i++)
        {
            // if (_count > 5)
            // {
            //     return;
            // }
            ball[i].gameObject.SetActive(true);
            ball[i].DamageSetup(_damage);
        }
        if (isFisrt && _count != 0)
        {
            StartCoroutine(RotateAndRest());
            isFisrt = false;
        }
    }

    IEnumerator RotateAndRest()
    {
        while (true)
        {
            yield return StartCoroutine(RotateForDuration(_rotationDuration));
            yield return new WaitForSeconds(_restDuration);
        }
    }

    IEnumerator RotateForDuration(float duration)
    {
        float elapsedTime = 0f;
        while (elapsedTime < duration)
        {
            if (isRotating)
            {
                transform.Rotate(Vector3.forward * _rotationSpeed * Time.deltaTime);
            }
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        ball.ForEach(item => item.gameObject.SetActive(false));
        yield return new WaitForSeconds(_restDuration);
        // Bật lại các game object của quả cầu
        Init();
    }
    public void StartRotation()
    {
        isRotating = true;
    }

    public void StopRotation()
    {
        isRotating = false;
    }
}
