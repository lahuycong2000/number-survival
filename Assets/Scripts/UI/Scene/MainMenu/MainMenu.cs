using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ensign;
using Ensign.Unity;
using TMPro;
namespace NumberSurvival
{
    public class MainMenu : MonoBehaviour
    {
        [SerializeField] private GUIButton _buttonPlay;
        [SerializeField] private TextMeshProUGUI _textCoin;

        private void OnEnable()
        {
            if (GameManager.Instance.UserData.coin < 0)
            {
                GameManager.Instance.UserData.coin = 0;
                GameManager.Instance.SaveData();
            }
            _textCoin.text = GameManager.Instance.UserData.coin.ToString();
            _buttonPlay.onClick += GameStart;
        }
        public void Start()
        {
            _textCoin.text = GameManager.Instance.UserData.coin.ToString();
        }
        private void OnDisable()
        {
            _buttonPlay.onClick -= GameStart;
        }
        public void GameStart()
        {
            GameManager.Instance.GameState = EGameState.StartGame;
        }
        private int index = 0;
        public void Cheat()
        {
            index++;
            if (index >= 10)
            {
                GameManager.Instance.UserData.coin = 10000;
                GameManager.Instance.SaveData();
                _textCoin.text = GameManager.Instance.UserData.coin.ToString();
            }
        }
    }
}

