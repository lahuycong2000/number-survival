using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ButtonCustom : MonoBehaviour
{
    [SerializeField] private Image _backGround;
    [SerializeField] private TextMeshProUGUI _valueCoin;
    [SerializeField] private TextMeshProUGUI _textButton;
    [SerializeField] private Color _whiteColor;
    [SerializeField] private Color _blackColor;
    public void ButtonBuy(int index, PlayerIcon spaceShip)
    {
         AudioManager.Instance.PlaySfx("ButtonClick");
        switch (index)
        {
            case 0: // Selected
                _backGround.color = _whiteColor;
                _textButton.text = "Selected";
                _valueCoin.gameObject.SetActive(false);
                break;
            case 1: // Unlock
                if (GameManager.Instance.UserData.shipIdSelect == spaceShip.PlayerIndex)
                {
                    _backGround.color = _whiteColor;
                    _textButton.text = "Selected";
                    _valueCoin.gameObject.SetActive(false);
                }
                else
                {
                    _backGround.color = _blackColor;
                    _textButton.text = "Unlock";
                    _valueCoin.gameObject.SetActive(true);
                }
                break;
        }
    }

}
