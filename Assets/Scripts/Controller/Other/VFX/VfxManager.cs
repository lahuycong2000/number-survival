using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Ensign.Unity;
namespace NumberSurvival
{
    public class VfxManager : MonoBehaviour
    {
        [SerializeField] private List<VfxController> _vfxEnemyDie;
        [SerializeField] private List<VfxController> _vfxLevelUp;
        public List<VfxController> VfxEnemyDie { get => _vfxEnemyDie; set => _vfxEnemyDie = value; }
        public List<VfxController> VfxLevelUp { get => _vfxLevelUp; set => _vfxLevelUp = value; }

        public void InitVFX()
        {
            VfxEnemyDie.ForEach(item => item.CreatePool<VfxController>(10));
            VfxLevelUp.ForEach(item => item.CreatePool<VfxController>(10));
        }
    }
}
