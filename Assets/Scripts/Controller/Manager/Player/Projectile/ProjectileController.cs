using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ensign.Unity;

using NumberSurvival;
public class ProjectileController : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private SpriteRenderer _image;
    [SerializeField] private EProjectile _type;
    public EProjectile Type { get => _type; set => _type = value; }
    private Transform _taget;
    private Vector3 _shootDir;
    private PlayerManager _player;
    [SerializeField] private float _damage;
    public bool isNormalBullet;
    public bool isLightBullet;
    public bool isBoss;
    public Transform Taget
    {
        get
        {
            return _taget;
        }
        set
        {
            _taget = value;
        }
    }
    public void DamageSetup(float damage)
    {
        _damage = damage;
    }
    public void SetUpDir(PlayerManager player, Vector3 shootDir)
    {
        _player = player;
        _shootDir = shootDir.normalized;
    }
    public void Update()
    {

        if (_type != EProjectile.ball)
        {
            transform.position += _shootDir * (_speed / 10f) * Time.deltaTime;
            if (Vector2.Distance(this.transform.position, _player.transform.position) > 5)
            {
                this.Recycle();
            }
            if (_image != null)
            {
                Vector2 direction = _shootDir.normalized;
                float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90f;
                _image.gameObject.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
                _image.gameObject.transform.Rotate(Vector3.forward * 1 * Time.deltaTime);
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (isBoss)
            return;
        Enemy enemy = other.gameObject.GetComponent<Enemy>();
        if (enemy && GameManager.Instance.GameState == EGameState.Playing)
        {
            enemy.TakeDamage(_damage);
            TextDamage txt = GameManager.Instance.TxtDamage.Spawn(enemy.canvas.transform);
            txt.Show(_damage, enemy.GetComponent<SpriteRenderer>().color);
            if (isNormalBullet)
            {
                this.Recycle();
            }
        }

    }

}
public enum EProjectile
{
    normal,
    railgun,
    ball,
    Lightning
}
