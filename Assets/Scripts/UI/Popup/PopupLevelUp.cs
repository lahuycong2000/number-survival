using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System;
using Ensign;
using Ensign.UniRx;
using TMPro;

namespace NumberSurvival
{
    public class PopupLevelUp : Popup
    {
        [SerializeField] private Button _btnReroll;
        [SerializeField] private TextMeshProUGUI _countReroll;
        [SerializeField] private List<ItemChooseSkill> _btn = new List<ItemChooseSkill>();

        private void Awake()
        {
            SetData();
        }
        private void Start()
        {
            AudioManager.Instance.PlaySfx("LevelUp");
            _btn.ForEach(item =>
            {
                item.AddListener(() => this.ClosePopup());
            });
        }
        private void OnEnable()
        {
            _btnReroll.onClick.AddListener(CheckReroll);
        }
        private void OnDisable()
        {
            _btnReroll.onClick.RemoveListener(CheckReroll);
            _btn.ForEach(item =>
            {
                item.RemoveListener(() => this.ClosePopup());
            });
        }
        public void CheckReroll()
        {
            if (GameManager.Instance.PlayerData.reroll != 0)
            {
                GameManager.Instance.PlayerData.reroll -= 1;
                SetData();
            }
        }
        private void SetData()
        {
            GameManager.Instance.NumberSkillGet.Shuffle();
            for (int i = 0; i < _btn.Count; i++)
            {
                int number = GameManager.Instance.NumberSkillGet[i];
                Skill skillManager = GameManager.Instance.DataSkill.skills[number];
                _btn[i].Init(skillManager);
            }
            _countReroll.text =  GameManager.Instance.PlayerData.reroll.ToString();
        }
        public void ClosePopup()
        {
            GameManager.Instance.GameState = EGameState.Playing;
            this.Close();
        }

    }
}
