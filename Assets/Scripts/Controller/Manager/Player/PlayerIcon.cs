using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
using UnityEngine.UI;
using NumberSurvival;
public class PlayerIcon : MonoBehaviour
{
    [SerializeField] private int _playerIndex;
    [SerializeField] private Image _playerImage;
    [SerializeField] private string _nameShip;
    [SerializeField] private Color _color;
    [SerializeField] private Color _colorWait;
    [SerializeField] private int _price;
    [SerializeField] private bool _unlocked;
    private Color _originColor;
    private Vector3 _localPos;
    public int PlayerIndex
    {
        get => _playerIndex;
    }
    public bool Unlocked
    {
        get => _unlocked;
        set => _unlocked = value;
    }
    public int Price
    {
        get => _price;
        set => _price = value;
    }
    public string NameShip
    {
        get => _nameShip;
        set => _nameShip = value;
    }
    private void Awake()
    {
        _localPos = this.GetComponent<RectTransform>().localPosition;
    }
    public void LoadIconPlayer(int playerIndex)
    {
        _playerIndex = playerIndex;
        string path = string.Format("Prefabs/space_ship_{0}", playerIndex);
        _playerImage.sprite = Resources.Load<Sprite>(path);
        _originColor = _playerImage.color;
        _playerImage.SetNativeSize();
    }
    public void SetHighlightScale(PlayerIcon currentItem)
    {
        this.transform.localScale = Vector3.one * (currentItem == this ? 2.5f : 1f);
        _playerImage.color = (currentItem == this ? _color : _colorWait);
        // this.GetComponent<RectTransform>().localPosition = (currentItem == this ? this.GetComponent<RectTransform>().localPosition + new Vector3(0f, -100f, 0f) : _localPos);
    }
}
