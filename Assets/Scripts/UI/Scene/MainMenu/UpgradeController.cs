using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Ensign.Unity;
using NumberSurvival;
using TMPro;
public class UpgradeController : MonoBehaviour
{
    private List<ItemUpgradeSkill> _items = new List<ItemUpgradeSkill>();
    [SerializeField] private RectTransform _content;
    [SerializeField] private ItemUpgradeSkill _itemUpgrade;
    [SerializeField] private TextMeshProUGUI _textCoin;

    private List<Skill> _dataSkill = new List<Skill>();
    public List<ItemUpgradeSkill> Items
    {
        get => _items;
        set => _items = value;
    }
    private void Start()
    {
        //for (int i = 0; i < GameManager.Instance.UserData.levelSkill.Length; i++)
        for (int i = 0; i < GameManager.Instance.DataUpdateSkill.skills.Length; i++)
        {
            if (i == GameManager.Instance.DataUpdateSkill.skills[i].skillID)
            {
                _dataSkill.Add(GameManager.Instance.DataUpdateSkill.skills[i]);
            }
        }
        SetCoinText();
        SetScroll();
        InitShop();
        // SetData();
    }
    public void SetScroll()
    {
        GridLayoutGroup grid = _content.GetComponent<GridLayoutGroup>();
        _content.SetHeight(_dataSkill.Count * grid.cellSize.y + grid.padding.bottom * 2 + grid.spacing.y * _dataSkill.Count);
    }
    public void InitShop()
    {
        Debug.Log("_dataSkill.Count : " + _dataSkill.Count);
        for (int i = 0; i < _dataSkill.Count; i++)
        {
            ItemUpgradeSkill buttonSelect = Instantiate(_itemUpgrade);
            buttonSelect.transform.SetParent(_content.transform);
            buttonSelect.SkillLoader(_dataSkill[i], true);
            _items.Add(buttonSelect);
        }
    }
    public void SetCoinText()
    {
        if (GameManager.Instance.UserData.coin < 0)
        {
            GameManager.Instance.UserData.coin = 0;
            GameManager.Instance.SaveData();
        }
        _textCoin.text = GameManager.Instance.UserData.coin.ToString();
    }
    // private void SetData()
    // {
    //     for (int i = 0; i < _dataSkill.Count; i++)
    //     {
    //         _item[i].SkillLoader(_dataSkill[i]);
    //     }
    // }
}
