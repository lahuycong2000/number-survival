using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using Ensign.Unity;

public class TextDamage : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _txtPoint;
    private Tween _fadeTween;
    public void Show(float point, Color color, bool isHeal = false)
    {
        if (isHeal)
        {
            _txtPoint.text = "+" + point.ToString("F0");
            _txtPoint.color = Color.green;
        }
        else
        {
            _txtPoint.text = point.ToString("F0");
            _txtPoint.color = color;
        }
        _txtPoint.transform.localPosition = new Vector3(0, 0, 0);
        _txtPoint.transform.DOLocalMoveY(0.5f, 1.3f).SetEase(Ease.Linear);
        Fade(0, 1f, () =>
        {
            this.Recycle();
        });
    }
    public void Fade(float endValue, float duration, TweenCallback onEnd)
    {
        _fadeTween = _txtPoint.DOFade(endValue, duration)
        .SetEase(Ease.Linear).SetDelay(0.3f);
        _fadeTween.onComplete += onEnd;
    }
}
