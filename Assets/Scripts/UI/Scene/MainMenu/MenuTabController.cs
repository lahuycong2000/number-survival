using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using TMPro;
using Ensign;
public class MenuTabController : MonoBehaviour
{
    [SerializeField] private EMainUITab _type;
    [SerializeField] private Image _icon;
    [SerializeField] private Image _backGround;
    [SerializeField] private GUIButton _button;

    [SerializeField] private Color _deactiveColor;
    [SerializeField] private Color _activeColor;
    [SerializeField] private Sprite _deactiveImage;
    [SerializeField] private Sprite _activeImage;
    private Action<EMainUITab> _onClicked;

    private void OnEnable()
    {
        AddAllButtonEvent();
    }
    private void OnDisable()
    {
        RemoveAllButtonEvent();
    }
    public void Initialize(Action<EMainUITab> onClicked)
    {
        _onClicked = onClicked;
    }
    private void AddAllButtonEvent()
    {
        _button.onClick += OnButtonClicked;
    }
    private void RemoveAllButtonEvent()
    {
        _button.onClick -= OnButtonClicked;
    }
    private void OnButtonClicked()
    {
         AudioManager.Instance.PlaySfx("ButtonClick");
        _onClicked?.Invoke(_type);
    }
    public void UpdateUI(EMainUITab type)
    {
        bool isFocus = _type == type;
        _icon.color = isFocus ? _activeColor : _deactiveColor;
        _backGround.sprite = isFocus ? _activeImage : _deactiveImage;
        _backGround.color = isFocus ? new Color(1,1,1,1) : new Color(1, 1, 1,0);
    }

}
