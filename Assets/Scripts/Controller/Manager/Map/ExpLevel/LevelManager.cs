using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace NumberSurvival
{
    public class LevelManager : MonoBehaviour
    {
        public const string PATH_LEVEL_CONFIG = "Data/LevelConfig";
        [SerializeField] private Slider _sliderLevel;
        [SerializeField] private TextMeshProUGUI _textLevel;
        //[SerializeField] private TextMeshProUGUI _waveText;
        [SerializeField] private float _currentExp;
        [SerializeField] private int _maxValueNextLevel;
        private int _minValueNextLevel;
        // private int _currentWave;
        //private int _maxWave;
        private LeveConfig _leveConfig = new LeveConfig();
        private int _currentLevel;
        public int CurrentLevel { get => _currentLevel; }

        void Start()
        {
            var dataJson = Resources.Load<TextAsset>(PATH_LEVEL_CONFIG);
            _leveConfig = JsonUtility.FromJson<LeveConfig>(dataJson.text);
            _currentLevel = 0;
            // _currentWave = 1;
            //_maxWave = _leveConfig.waves.Length;
            _maxValueNextLevel = _leveConfig.level[_currentLevel];
            SliderHandle(_maxValueNextLevel);
            _textLevel.text = _currentLevel.ToString();
            //ReloadUI();
        }
        public void AddExp(float exp)
        {
            if (_currentLevel < 34)
            {
                _currentExp = _currentExp + exp;
                _sliderLevel.value += exp;
                if (_currentExp > _maxValueNextLevel)
                {
                    _currentLevel++;
                    if (_currentLevel > 0)
                        _sliderLevel.minValue = _leveConfig.level[_currentLevel - 1];
                    _maxValueNextLevel = _leveConfig.level[_currentLevel];
                    SliderHandle(_maxValueNextLevel);
                    _sliderLevel.value = 0;
                    ChoseSkillLevelUp();
                    Debug.LogWarning("_sliderLevel.value : " + _currentExp + "_maxValueNextLevel : " + _maxValueNextLevel);

                }
                _textLevel.text = _currentLevel.ToString();
            }
            else
            {
                if (GameManager.Instance.IsBoss)
                {
                    GameManager.Instance.SpawnBoss();
                    _textLevel.text = "Max";
                    GameManager.Instance.IsBoss = false;
                }
            }
        }
        // public void ReloadUI()
        // {
        //     if (GameManager.Instance.CurrentEnemyKilled > _leveConfig.waves[_currentWave - 1])
        //     {
        //         _currentWave++;
        //         WaveTextFomat(_currentWave);
        //     }
        // }
        public void SliderHandle(int maxValue)
        {
            _sliderLevel.maxValue = maxValue;
        }
        public void ChoseSkillLevelUp()
        {
            // GameManager.Instance.VFXManager.VfxLevelUp.SpawnVFX(GameManager.Instance.PlayerManager.transform.position);
            if (GameManager.Instance.GameState != EGameState.EndGame)
            {
                Popup popup = PopupOpenerSingleton.Instance.OpenPopup<PopupLevelUp>("UI/Popup/LevelUp/PopupLevelUp");
                GameManager.Instance.GameState = EGameState.PauseGame;
            }
        }
        // public void WaveTextFomat(int currentWave)
        // {
        //     string textWave = string.Format(currentWave.ToString() + "/{0}", _maxWave.ToString());
        //     _waveText.text = textWave;
        // }
    }

    public class LeveConfig
    {
        public int[] level;
        public int[] waves;
    }
}
