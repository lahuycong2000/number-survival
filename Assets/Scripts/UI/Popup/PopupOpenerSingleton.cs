using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Ensign.Unity;

[Resource("UI/PopupSigleton/CanvasOpenerSingleton",IsDontDestroy = false)]
public class PopupOpenerSingleton : Singleton<PopupOpenerSingleton>
{
    [SerializeField] private Canvas _canvas;

    public virtual T OpenPopup<T>(string prefabPath) where T : Popup
    {
        Popup prefab = Resources.Load<Popup>(prefabPath);
        Popup popup = Instantiate(prefab, _canvas.transform);
        popup.Open(_canvas);
        return (T)popup;
    }
}
