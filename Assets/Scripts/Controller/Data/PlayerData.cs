using System;
using UnityEngine;
[Serializable]
public class PlayerData
{
    public float playerLevel;
    public bool playerDie;
    public float playerDamage;
    public float playerSpeed;
    public float playerMaxHealth;
    public float playerCurrenHealth;
    public float distanceGetEXP;
    public float distanceShootEnemy;
    public float timeFireRate;
    public float armor;
    public float expBonus;
    public float coinBonus;
    public float timeFireRateRailGun;
    public float damageRailGun;
    public float reroll;
    public float healthIndex;
}
