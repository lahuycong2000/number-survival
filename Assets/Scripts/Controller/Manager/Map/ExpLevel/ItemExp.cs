using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Ensign.Unity;
using Ensign.Tween;
using NumberSurvival;
public class ItemExp : MonoBehaviour
{
    [SerializeField] private int _value;
    [SerializeField] private EExpType _expType;
    [SerializeField] private float _timeMove;

    [SerializeField] private LayerMask _playerLayer;

    public EExpType ExpType
    {
        get => _expType;
        set => _expType = value;
    }
    private bool _wasEating;

    private void OnEnable()
    {
        _wasEating = false;
    }

    private void OnDisable()
    {
        this.gameObject.TweenCancel();
        StopAllCoroutines();
    }

    public void MoveToPlayer(PlayerManager player)
    {
        if (_wasEating) return;
        _wasEating = true;
        this.transform.SetParent(player.transform);
        this.gameObject.TweenMoveLocal(Vector3.zero, _timeMove)
        .setEase(LeanTweenType.easeInOutBack);
    }
    public void RecycleItem()
    {
        GameManager.Instance.CurrentListExp.Remove(this);
        this.Recycle();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if ((_playerLayer.value & (1 << other.gameObject.layer)) > 0)
        {
            if (this._expType == EExpType.Coin)
            {
                GameManager.Instance.CoinEarned += _value;
                GameManager.Instance.CoinEarned += GameManager.Instance.PlayerManager.PlayerData.coinBonus;
                this.RecycleItem();
            }
            else
            {
                GameManager.Instance.LevelManager.AddExp(_value + GameManager.Instance.PlayerManager.PlayerData.expBonus);
                this.RecycleItem();
            }
        }
    }
}
public enum EExpType
{
    Silver,
    Gold,
    Coin
}
