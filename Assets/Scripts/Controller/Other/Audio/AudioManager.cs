using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;
    [SerializeField] private Sound[] _musicSounds, _sfxSounds;
    [SerializeField] private AudioSource _musicSource;
    [SerializeField] private AudioSource _sfxSource;
    
    public void Awake()
    {
        Instance = this;
    }
    public void Start()
    {
        if (PlayerPrefs.HasKey("music"))
        {
            _musicSource.volume = PlayerPrefs.GetFloat("music");
            _sfxSource.volume = PlayerPrefs.GetFloat("sfx");
            
        }
        else
        {
            _musicSource.volume = 1;
            _sfxSource.volume = 1;
        }
        PlayMusic("Theme");
    }
    public void PlayMusic(string nameMusic)
    {
        Sound sound = Array.Find(_musicSounds, m => m.name == nameMusic);

        if (sound == null)
        {
            Debug.Log("sound not found");
        }
        else
        {
            _musicSource.clip = sound.clip;
            _musicSource.Play();
        }
    }
    public void PlaySfx(string nameSfx)
    {
        Sound sound = Array.Find(_sfxSounds, m => m.name == nameSfx);

        if (sound == null)
        {
            Debug.Log("sound not found");
        }
        else
        {
            _sfxSource.PlayOneShot(sound.clip);
        }
    }
    public void PlaySfxLoop(string nameSfx)
    {
        Sound sound = Array.Find(_sfxSounds, m => m.name == nameSfx);

        if (sound == null)
        {
            Debug.Log("sound not found");
        }
        else
        {
            if (!_sfxSource.isPlaying)
            {
                _sfxSource.PlayOneShot(sound.clip);

            }
            else
            {
            }
        }
    }
  
    public void MusicVolume(float volume)
    {
        _musicSource.volume = volume;
    }
    public void SfxVolume(float volume)
    {
        _sfxSource.volume = volume;
    }

    [System.Serializable]
    public class Sound
    {
        public string name;
        public AudioClip clip;
    }
}
