using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayUIManager : MonoBehaviour
{

    [Header("UI")]
    [SerializeField] private List<MenuTabController> _missionTabControllers;

    [SerializeField] private Transform _chooseSpaceShip;
    [SerializeField] private Transform _mainPlay;
    [SerializeField] private Transform _upgrade;
    private EMainUITab _currentTab;
    private EMainUITab CurrentTab
    {
        get => _currentTab;
        set
        {
            _currentTab = value;
            UpdateUI(_currentTab);
            switch (_currentTab)
            {
                case EMainUITab.ChooseSpaceShip:

                    break;
                case EMainUITab.Upgrade:
                    // UpdateGetMissionUI();
                    break;
            }
        }
    }
    private void Start()
    {
        _missionTabControllers.ForEach(tab =>
        {
            tab.Initialize(ChangeTab);
        });
    }
    private void ChangeTab(EMainUITab data)
    {
        CurrentTab = data;
    }
    private void OnEnable()
    {
        ChangeTab(EMainUITab.MainScene);
    }
    private void OnDisable()
    {
    }
    private void UpdateUI(EMainUITab tab)
    {
        _missionTabControllers.ForEach(tabController =>
        {
            tabController.UpdateUI(_currentTab);
        });
        _chooseSpaceShip.gameObject.SetActive(tab == EMainUITab.ChooseSpaceShip);
        _mainPlay.gameObject.SetActive(tab == EMainUITab.MainScene);
        _upgrade.gameObject.SetActive(tab == EMainUITab.Upgrade);
    }
}
public enum EMainUITab
{
    ChooseSpaceShip,
    MainScene,
    Upgrade
}
