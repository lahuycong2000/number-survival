using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;
using System;
using Ensign;
using Ensign.UniRx;
using NumberSurvival;
public class PausePopup : Popup
{
    [SerializeField] private Button _btnHome;
    [SerializeField] private Button _btnResume;
    [SerializeField] private List<ItemPause> _listItem = new List<ItemPause>();
    public void Start()
    {
        for (int i = 0; i < _listItem.Count; i++)
        {
            Skill skill = GameManager.Instance.DataSkill.skills[i];
            _listItem[i].InitPause(GameManager.Instance.LoadImage(skill.nameSkill),
            GameManager.Instance.GetDataSkill(skill.nameSkill).lvl.ToString());
        }
    }
    private void OnEnable()
    {
        _btnHome.onClick.AddListener(() =>
       {
           //GameManager.Instance.GameState = EGameState.MainMenu;
           GameManager.Instance.Reset();
           this.Close();
       });
        _btnResume.onClick.AddListener(() =>
       {
           GameManager.Instance.GameState = EGameState.Playing;
           this.Close();
       });
    }
    private void OnDisable()
    {
        _btnHome.onClick.RemoveListener(() =>
      {
          this.Close();
      });
        _btnResume.onClick.RemoveListener(() =>
       {
           this.Close();
       });
    }
    public override void Close()
    {
        base.Close();
    }
}
